# Aurelia single cell data

In order to analyze the Aurelia single cell data, we need a transcriptome index. What to use is a good question, as there are now 3 genomes and they have various qualities:

| genome   | n_scaffolds | n_contigs | scaf_bp   | contig_bp | gap_pct | scaf_N50 | scaf_L50 | ctg_N50 | ctg_L50 | gc_avg  | gc_std  | unique_map |
|----------|-------------|-----------|-----------|-----------|---------|----------|----------|---------|---------|---------|---------|------------|
| atlantic | 2709        | 19288     | 376932402 | 351956122 | 6.626   | 112      | 1042981  | 2912    | 33957   | 0.37104 | 0.02187 | 27.1 %     |
| pacific  | 7715        | 47801     | 426022005 | 364427511 | 14.458  | 628      | 181220   | 8297    | 12474   | 0.37533 | 0.03561 | 60.7 %     |
| sandiego | 25454       | 76361     | 757170055 | 659941823 | 12.841  | 1766     | 121658   | 9639    | 18990   | 0.37476 | 0.03168 | 85.0 %     |

In a [recent paper](https://doi.org/10.1038/s41559-019-0853-y), the genomes of two *A. aurita* subspecies, pacific or Roscoff, also laboratory, and Baltic. The latter was taken as the "reference" because for some reason the Roscoff strain was not of as high a quality. T  hese two genomes were downloaded on our `evo` server into `/evo/data/seqdb/aurelia_aurita/{atlantic,pacific}`. Additionally, there was another [another paper](https://doi.org/10.1038/s41559-018-0719-8) prior to this. This was taken from Birch Aquarium (San Diego, Califo  rnia) and grown into a clonal colony. This was downloaded from the [google drive associated with the paper](https://drive.google.com/drive/folders/1NC6bZ9cxWkZyofOsMPzrxIH3C7m1ySiu) and placed into `/evo/data/seqdb/aurelia_aurita/sandiego`.

Although the atlantic assembly is the best assembly, the reads from our project map much worse than pacific and san diego. These sequences were downloaded and used to make cellranger genomes. So far we only have an annotation for the San Diego genome:

```
python3 scripts/gff3_to_gtf.py data/sandiego/Aurelia.Genome_Annotation_v1.2_11-28-18.gff3 > data/sandiego/Aurelia.Genome_Annotation_v1.2_11-28-18.gtf
cellranger mkgtf data/sandiego/Aurelia.Genome_Annotation_v1.2_11-28-18.gtf data/sandiego/Aurelia.Genome_Annotation_v1.2_11-28-18.filtered.gtf --attribute=pseudo:false
```

In an email with Konstantin Khalturin, we received the locations of the other annotations [atlantic](https://marinegenomics.oist.jp/aurelia_aurita/viewer/download?project_id=69) and [pacific](https://marinegenomics.oist.jp/aurelia_aurita_pacific/viewer/download?project_id=75). He also noted the following relevant points:

> Please keep in mind that the genome assembly of the Baltic strain is of much higher quality than that of Roscoff. The latter one was just a side product of our genome project and its continuity and gene models are not good. Thus, to my mind, it makes not much sense to use Roscoff assembly gene models. Mapping against good reference transcriptome assembly (https://www.ncbi.nlm.nih.gov/nuccore/GHAI00000000) is a much better idea.

He also made reference to a new assembly from the Pacific strain near the Nagasaki region, quite close to Roscoff (Pacific) and in much better shape. He has offered to let us try mapping our reads on a couple of scaffolds and if it works, that we could then use the whole genome for our project. Currently holding off on that.

For the other conversions, we just use `gffread`:


```
gffread -T data/atlantic/AUR21_r04_wref.gff3 -o data/atlantic/AUR21_r04_wref.gtf 
gffread -T data/pacific/ARSv1_genome_assembly.gff3 -o data/pacific/ARSv1_genome_assembly.gtf
```

## Extending the Ends of Genes

The script used requires a chrom sizes file, which varies from case to case in approach.

```
fasta_stat data/atlantic/AUR21_r04_250316.fa | head -n -2 | awk '{print $2,$1}' > data/atlantic/AUR21_r04_250316.chrom_sizes
cat data/pacific/ARSv1_genome_assembly.fa | grep '^>'  | sed -e 's/>\([[:alnum:]]\+\)[[:space:]]\+len=\([[:digit:]]\+\)$/\1 \2/' > data/pacific/ARSv1_genome_assembly.chrom_sizes
fasta_stat data/sandiego/Aurelia.Genome_v1.2_11-27-18.fasta | head -n -2 | awk '{print $2,$1}' > data/sandiego/Aurelia.Genome_v1.2_11-27-18.chrom_sizes
```

```
EXT=500
python3 scripts/elongate_gtf_genes.py processing/atlantic/AUR21_r04_wref.gtf processing/atlantic/AUR21_r04_250316.chrom_sizes $EXT > processing/atlantic/AUR21_r04_wref.extended.gtf
python3 scripts/elongate_gtf_genes.py processing/pacific/ARSv1_genome_assembly.gtf processing/pacific/ARSv1_genome_assembly.chrom_sizes $EXT > processing/pacific/ARSv1_genome_assembly.extended.gtf
python3 scripts/elongate_gtf_genes.py processing/sandiego/Aurelia.Genome_Annotation_v1.2_11-28-18.gtf processing/sandiego/Aurelia.Genome_v1.2_11-27-18.chrom_sizes $EXT > processing/sandiego/Aurelia.Genome_Annotation_v1.2_11-28-18.extended.gtf
```

## Make the genomes

Now make the genomes. `mkref` is not friendly to the cluster since you cannot use a slash in the genome name. Simply run one at a time

```
cellranger mkref --genome auraur.sandiego --fasta data/sandiego/Aurelia.Genome_v1.2_11-27-18.fasta --genes processing/sandiego/Aurelia.Genome_Annotation_v1.2_11-28-18.extended.gtf
cellranger mkref --genome auraur.atlantic --fasta data/atlantic/AUR21_r04_250316.fa --genes processing/atlantic/AUR21_r04_wref.extended.gtf
cellranger mkref --genome auraur.pacific --fasta data/pacific/ARSv1_genome_assembly.fa --genes processing/pacific/ARSv1_genome_assembly.extended.gtf
cellranger mkref --genome auraur.41k --fasta data/41k_reduced_set/trinity_151109not-trimmed.Trinity.cdhit.fasta --genes data/41k_reduced_set/trinity_151109not-trimmed.Trinity.cdhit.gtf
```

## *De novo* transcriptomes

`data/41k_reduced_set` is from the stefan archive, sitting in `/archive/proj/20180722-jahnel.tar.gz`. within there is the  directory `aurelia/results/41k_reduced_set/`. This is reduced from the original transcriptome which had 329350, which was placed in 
`data/trinity_151109not-trimmed.Trinity.fasta`

Now we take the denovo transcriptome reduced set and turn that into a cellranger reference.

```
python3 scripts/fasta_to_gtf.py data/41k_reduced_set/trinity_151109not-trimmed.Trinity.cdhit.fasta > data/41k_reduced_set/trinity_151109not-trimmed.Trinity.cdhit.gtf
cellranger mkref --genome auraur.41k_reduced --fasta data/41k_reduced_set/trinity_151109not-trimmed.Trinity.cdhit.fasta --genes data/41k_reduced_set/trinity_151109not-trimmed.Trinity.cdhit.star.gtf
```

I then attempted to make new transcriptomes with different filtering settings:

```
for c in 0.8 0.9 1.0; do 
    echo "cd-hit -c $c -i {data/trinity_151109not-trimmed.Trinity.fasta:i} -o {transcriptomes/trinity_151109not-trimmed.Trinity.cd-hit$c.fasta:o}"
done | tmprewrite | slurmtasks --name cdhit --preset log --mem 8 | sbatch
```

## Testing

Run cellranger on all the genomes. Given the nature of how cellranger works, have to be a little hacky about our `tmprewrite` command:

```
for g in 41k_reduced_set atlantic pacific sandiego; do
    echo "echo {aau_10x_$g:o} {auraur.$g:i} {data/HMTMKAFXY_all/HMTMKAFXY:i}; cd \$TMPDIR; cellranger count --id=aau_10x_$g --transcriptome=auraur.$g --fastqs=HMTMKAFXY --sample=96477 --expect-cells=5000 --localcores=8; cd -"
done | tmprewrite | slurmtasks --preset 8core,log --name aacrcount | sbatch

for g in 41k_reduced_set atlantic pacific sandiego; do
    echo "echo {aau_SW_$g:o} {auraur.$g:i} {data/HMTMKAFXY_all/HMTMKAFXY:i}; cd \$TMPDIR; cellranger count --id=aau_SW_$g --transcriptome=auraur.$g --fastqs=HMTMKAFXY --sample=96476 --expect-cells=5000 --localcores=8; cd -"
done | tmprewrite | slurmtasks --preset 8core,log --name aacrcount | sbatch

```
