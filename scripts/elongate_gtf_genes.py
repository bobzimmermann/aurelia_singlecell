#! /usr/bin/env python

import itertools
import click
#from pylib import IntervalTree

def overlap(a1,a2,b1,b2):
    return a1 < b2 and b1 < a2
    #return max(0,min(a2,b2)-max(a1,b1))

class Interval(object):
    def __init__(self,chrom,start,end,name,score,strand):
        self.chrom = chrom
        self.start = int(start)
        self.end = int(end)
        self.name = name
        self.strand = strand
    def overlap(self,target):
        if overlap(self.start,self.end,target.start,target.end) > 0:
            if self.strand == target.strand:
                return 'repeat_sense'
            else:
                return 'repeat_antisense'
        else:
            return None

def maybe_parse_int(s):
    try:
        result = int(s)
        return result
    except ValueError:
        return s

class Feature:
    def __init__(self,seqname,source,feature,start,end,score,strand,frame,attrs):
        (self.seqname, self.source, self.feature, self.start, self.end,
                self.score, self.strand, self.frame) = [maybe_parse_int(f) for f in [
                    seqname,source,feature,start,end,score,strand,frame]]
        self.attrs = dict([x.strip().strip('"') 
                           for x in a.split('"', 1)] 
                           for a in attrs.strip(';').split(';'))

    def __repr__(self):
        return '\t'.join(str(f) for f in [
            self.seqname, self.source, self.feature, 
            self.start, self.end, self.score, self.strand, self.frame, 
            self._attrs_repr()]
        )

    def _attrs_repr(self):
        return "; ".join(f'{k} "{v}"' for k,v in self.attrs.items())


    def updated(self, **kwargs):
        """return a new identical feature with the requested attributes altered"""
        pass_kwargs = {
            k: kwargs.get(k, getattr(self, k))
            for k in ["seqname", "source", "feature", "start", "end", "score",
                      "strand", "frame"]
            }
        pass_kwargs["attrs"] = self._attrs_repr()
        return Feature(**pass_kwargs)


class Gene:
    def __init__(self):
        self.chrom = None
        self.start = None
        self.end = None
        self.strand = None
        self.features = []
        self.extended = False

    def add_feature(self, feature):
        if self.chrom is not None and feature.seqname != self.chrom:
            raise ValueError(
                f"Cannot add feture with seqname {feature.seqname} " +
                f"to gene on seqname {self.chrom}")

        self.features.append(feature)

        if self.chrom is None:
            self.chrom = feature.seqname

        if self.start is None:
            self.start = feature.start
        else: 
            self.start = min(self.start, feature.start)

        if self.end is None:
            self.end = feature.end
        else:
            self.end = max(self.end, feature.end)

        if self.strand is None:
            self.strand = feature.strand
        else:
            if self.strand != feature.strand:
                raise ValueError(f"Feature strand does not match gene strand {feature}")

    def extend_end(self,new_end):
        self.features = [f.updated(end=new_end) if f.end == self.end else f
                         for f in self.features]
        self.end = new_end
        self.extended = True

    def extend_start(self,new_start):
        self.features = [f.updated(start=new_start) if f.start == self.start else f
                         for f in self.features ]
        self.start = new_start
        self.extended = True
    
    def __repr__(self):
        return "\n".join(repr(f) for f in self.features)

@click.command()
@click.argument("GTF")
@click.argument("CHROM_SIZES")
@click.argument("EXTENSION", type=int)
def main(gtf, chrom_sizes, extension):
    genes = dict()
    with open(gtf) as f:
        for line in f:
            fields = line.strip().split("\t")
            if len(fields) != 9 or line.strip().startswith("#"):
                continue
            feature = Feature(*fields)
            gene = genes.setdefault(feature.attrs["gene_id"], Gene())
            gene.add_feature(feature)

    chroms = dict()
    for gene in genes.values():
        chroms.setdefault(gene.chrom, []).append(gene)

    chroms = { k:list(sorted(v,key=lambda g: g.start))
               for k,v in chroms.items()}

    ref_sizes = {l.split()[0]:int(l.split()[1]) 
                 for l in open(chrom_sizes)
                 if len(l.split()) > 0}

    def find_next_gene(this_gene,next_genes):
        for gene in next_genes:
            if gene.start > this_gene.end:
                return gene
        return None

    for ref,gene_list in chroms.items():
        for i,gene in enumerate(gene_list):
            if gene.extended:
                continue
            if i > 0 and gene_list[i-1].start < gene.start \
               and gene_list[i-1].end > gene.end:
                continue # dominated gene; ignore
            if gene.strand == '-':
                if i > 0 and \
                        gene_list[i-1].end < gene.start and \
                        gene_list[i-1].strand == '-':
                    gene.extend_start(max(1,gene_list[i-1].end,gene.start-extension))
                else: 
                    gene.extend_start(max(1,gene.start-extension))
            else:
                next_gene = find_next_gene(gene,gene_list[i+1:])
                if next_gene is None:
                    gene.extend_end(min(ref_sizes[ref],gene.end+extension))
                elif next_gene.strand == '+':
                    gene.extend_end(min(ref_sizes[ref],next_gene.start-1,gene.end+extension))
                else: # genes are convergent, always extend even if they overlap
                    next_gene.extend_start(max(1,next_gene.start-extension))
                    gene.extend_end(min(ref_sizes[ref],gene.end+extension))


    for ref,gene_list in chroms.items():
        for gene in gene_list:
            print(str(gene))

if __name__ == "__main__":
    main()
