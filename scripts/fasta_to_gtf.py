#! /usr/bin/env python

from Bio import SeqIO
import click

@click.command()
@click.argument("FASTA")
def main(fasta):
    with open(fasta) as f:
        for seq in SeqIO.parse(f, "fasta"):
            print("\t".join(
                [seq.name, "fasta", "CDS", "1", str(len(seq)), ".", "+", "0",
                    f'gene_id "{seq.name}"; transcript_id "{seq.name}"']
                ))

if __name__ == "__main__":
    main()

