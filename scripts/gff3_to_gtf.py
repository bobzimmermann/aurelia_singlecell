#! /usr/bin/env python

from dataclasses import dataclass
import click

@dataclass
class GeneInfo:
    gene_id: str
    protein_id: str = None
    transcript_id: str = None
    product: str = None
    pseudo: str = "false"

    @classmethod
    def from_gff3_attrs(cls, attrs):
        converter = {"Parent": "gene_id", "ID": "gene_id", "Name": "transcript_id"}
        converted_attrs = {converter.get(k,k): v for k, v in attrs.items()}
        if converted_attrs["gene_id"].startswith("Gene."):
            converted_attrs["gene_id"] = converted_attrs["gene_id"].replace("Gene.", "")
        return cls(**converted_attrs)

    @classmethod
    def from_gff3_attr_string(cls, s):
        attrs = dict(a.split("=") for a in s.split(";"))
        return cls.from_gff3_attrs(attrs)

    def updated(self, other):
        self.protein_id = self.protein_id or other.protein_id
        self.transcript_id = self.transcript_id or other.transcript_id
        self.product = self.product or other.product
        is_pseudo = self.pseudo == "true" or other.pseudo == "true"
        self.pseudo = "true" if is_pseudo else "false"
        return self

    def __repr__(self):
        return "; ".join(
                '{} "{}"'.format(k, getattr(self, k).strip('"'))
                for k in self.__dataclass_fields__
                if getattr(self, k) is not None)

def preprocess_line(line):
    if line.startswith("#"):
        return None
    fields = line.strip().split("\t")
    if fields[2] in ["mRNA", "transcript", "gene", "exon", "CDS"]:
        return fields
    else:
        return None

def extract_gene_info(fields):
    return GeneInfo.from_gff3_attr_string(fields[-1])

def process_line(fields, gene_info_dict):
    cur_info = extract_gene_info(fields)
    fields[-1] = str(gene_info_dict[cur_info.gene_id])
    return "\t".join(fields)


@click.command()
@click.argument("GFF3")
def main(gff3):
    with open(gff3) as f:
        field_lists = [preprocess_line(line) 
                       for line in f
                       if preprocess_line(line) is not None]

        gene_infos = [extract_gene_info(fields) for fields in field_lists]
    gene_info_dict = {}
    for gene_info in gene_infos:
        cur_gene_info = gene_info_dict.get(gene_info.gene_id, gene_info)
        gene_info_dict[gene_info.gene_id] = cur_gene_info.updated(gene_info)

    print("\n".join(process_line(fields, gene_info_dict)
                    for fields in field_lists))


if __name__ == "__main__":
    main()

